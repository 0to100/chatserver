const axios = require('axios');

module.exports = function initTwitch() {
     axios.post(`https://id.twitch.tv/oauth2/token?client_id=${process.env.CLIENT_ID}&client_secret=${process.env.CLIENT_SECRET}&grant_type=client_credentials`)
         .then(res => {
            console.log('Recived Credentials')
            initFollowHooks(res.data.access_token)
        }).catch(err => {
            console.log(err)
        })
}

 async function unsubscribe(token) {
        axios.post('https://api.twitch.tv/helix/webhooks/hub', {
            'hub.callback': process.env.CALLBACK,
            'hub.mode': 'unsubscribe',
            'hub.topic': 'https://api.twitch.tv/helix/users/follows?first=1&to_id=471064546',
        }, {
            headers: {
                'Client-ID': process.env.CLIENT_ID,
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        })
            .then(() => {
                console.log('Unsubscribed')
            })
            .catch(err => {
                console.error(err)
            })
    }

    async function initFollowHooks(token) {
        await unsubscribe(token)
        axios.post('https://api.twitch.tv/helix/webhooks/hub', {
            'hub.callback': process.env.CALLBACK,
            'hub.mode': 'subscribe',
            'hub.topic': 'https://api.twitch.tv/helix/users/follows?first=1&to_id=471064546',
            'hub.lease_seconds': 864000,
            'hub.secret': process.env.TWITCH_SECRET
        }, {
            headers: {
                'Client-ID': process.env.CLIENT_ID,
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        })
            .then(() => {
                console.log('Follow Hook Initalized')
            })
            .catch(err => {
                console.error(err)
            })
    }