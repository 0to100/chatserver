const cors = require('cors')
const tmi = require('tmi.js');

const corsOptions = {
  origin: ['https://0to100.ink', 'https://overlays.0to100.ink'],
  optionsSuccessStatus: 200
}

const bot = new tmi.Client({
	connection: {
		secure: true,
		reconnect: true
	},
	identity: {
		username: 'CodePfleger',
		password: '' + process.env.TWITCH_OAUTH_TOKEN
	},
    channels: ['0to100ink']
});

bot.connect();

module.exports = function initServer(app, io, knex) {
    app.get('/follow', (req, res) => {
        console.log('Twitch verification')
        res.send(req.query['hub.challenge']);
    })

    app.post('/follow', async (req, res) => {
        if (req.verified && req.body && req.body.data) {
            const data = req.body.data
            data.forEach(async follow => {
                const row = randomNumber(20, 55)
                const seats = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J']
                const ticketData = {
                    userId: follow.from_id,
                    date: new Date().toLocaleString('en-US', {
                        weekday: 'long',
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric',
                        hour: '2-digit',
                        minute: '2-digit',
                        hour12: false,
                    }),
                    flightNumber: 'WV-8291',
                    seat: `${row}${seats[randomNumber(0, seats.length - 1)]}`,
                    gate: '12',
                    name: follow.from_name,
                    destination: '100',
                    departure: '0',
                    airline: 'Streamerlines',
                    type: 'follow'
                }

                await knex('tickets').insert(ticketData);

                bot.whisper(follow.from_name, `Danke, dass du Teil der Reise bist. Dein Follow Ticket, welches du grade on Screen gesehen hast, kannst du dir auch herunterladen: https://0to100.ink/tickets/?id=${follow.from_id}`)

                io.emit('ticket', {
                    ticketData
                })

                io.emit('follow', {
                    name: follow.from_name,
                    userId: follow.from_id,
                    followDate: follow.followed_at
                })
            });
        }
        res.send()
    })

    app.get('/tickets/:id', cors(corsOptions), async (req, res) => {
        const data = await knex('tickets').select().where('userId', req.params.id)
        if (data) {
            res.json(data)
        } else {
            res.error('No data found')
        }
        
    })

    function randomNumber(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min
    }
}