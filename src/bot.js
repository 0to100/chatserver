const tmi = require('tmi.js');

module.exports = function initBot() {
	const bot = new tmi.Client({
		connection: {
			secure: true,
			reconnect: true
		},
		identity: {
			username: 'CodePfleger',
			password: '' + process.env.TWITCH_OAUTH_TOKEN
		},
		channels: ['0to100ink']
	});

	bot.connect();

	bot.on('message', (channel, tags, message, self) => {
		// Ignore echoed messages.
		if(self) return;

		message = message.toLowerCase();

		switch (message) {
			case '!commands':
				bot.say(channel, `Eine Liste von Commands findest du hier: https://0to100.ink/blog/twitch-commands/`);
				break;
			case '!cocktails':
				bot.say(channel, `Cocktails ist eine App, welche wir im Stream gebaut haben. Lade sie dir jetzt im AppStore oder PlayStore herunter: https://apps.apple.com/us/app/minimal-cocktails/id1546888686 https://play.google.com/store/apps/details?id=ink.wolves.cocktails`);
				break;
			case '!website': ;
			case '!homepage':
				bot.say(channel, `Mehr erfahren kannst du auf: https://0to100.ink/`);
				break;
			case '!games': ;
			case '!spiele':
				bot.say(channel, `Unsere Twitch Spiele Seite findest du unter: https://flags.0to100.ink`);
				break;
			case '!blog':
				bot.say(channel, `Meinen Blog findest du unter: https://0to100.ink/blog/`);
				break;
			case '!twitter':
				bot.say(channel, `Einfach hier klicken: https://twitter.com/marcelxpfeifer`);
				break;
			case '!ig': ;
			case '!instagram':
				bot.say(channel, `Hier gehts zu Instragram: https://instagram.com/marcelxpfeifer`);
				break;
			case '!uses': ;
			case '!equipment': 
				bot.say(channel, `Das ist mein Zeug: https://0to100.ink/uses/`);
				break;
			case '!git': ;
			case '!gitlab': 
				bot.say(channel, `Alle unsere Projekte liegen auf GitLab. https://gitlab.com/0to100`);
				break;
			case '!hosting': 
				bot.say(channel, `Für static Hosting benutze ich häufig Surge: https://surge.sh für andere Dinge benutzt ich All-Inkl (Partnerlink): https://all-inkl.com/PA44DDC7C5F5675 oder Hetzner (Partnerlink): https://hetzner.cloud/?ref=dQDPThEXNAM1`);
				break;
		}
	});
}
