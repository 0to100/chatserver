const tmi = require('tmi.js');
const cron = require('node-cron');

module.exports = function initChat(io) {
	let chatters = [];
	const chat = new tmi.Client({
		connection: {
			secure: true,
			reconnect: true
		},
		channels: ['0to100ink']
	});

	chat.connect();

	chat.on('message', (channel, tags, message, self) => {
		const data = {
			channel,
			tags,
			message,
			self
		}
		if (chatters.indexOf(tags.username) === -1) {
			io.emit('firstMessage', tags['display-name']);
			chatters.push(tags.username)
		}

		if (message.startsWith('!')) {
			// Commands
			if (message.startsWith('!ttt')) io.emit('ttt', data);
		} else {
			// Message
			io.emit('message', data);
		}
	});

	chat.on("messagedeleted", (channel, username, deletedMessage, userstate) => {
		const messageId = userstate["target-msg-id"];

		io.emit('messagedeleted', messageId)
	});

	chat.on("raided", (channel, username, viewers) => {
		io.emit('raid', {
			raider: username,
			viewers: viewers
		})
	});

	// CLEAN CHATTERS LIST EVERY DAY
	cron.schedule('0 0 * * *', () => {
		chatters = []
	});
}