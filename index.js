require('dotenv').config()
const app = require('express')();
const crypto = require('crypto');
const bodyParser = require("body-parser");
const http = require('http').createServer(app);
const io = require('socket.io')(http, {
  cors: {
    origin: process.env.ORIGIN,
    methods: ["GET", "POST"],
  }
});
const knex = require('knex')({
  client: 'mysql',
  connection: {
    host : process.env.DATABASE_HOST,
    user : process.env.DATABASE_USER,
    password : process.env.DATABASE_PASSWORD,
    database : process.env.DATABASE_DB
  }
});
const cron = require('node-cron');
const initBot = require('./src/bot')
const initChat = require('./src/chat')
const initTwitch = require('./src/twitch')
const initServer = require('./src/server')

const verify = (req, res, buf, encoding) => {
    const expected = req.headers['x-hub-signature'];
    const calculated = 'sha256=' + crypto.createHmac('sha256', process.env.TWITCH_SECRET).update(buf).digest('hex');

    req.verified = expected === calculated;
};

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ verify: verify }));

initServer(app, io, knex)
initTwitch()

cron.schedule('0 0 * * *', () => {
    console.log('Reinit Twitch')
    initTwitch();
});

initBot()
initChat(io)

http.listen(2222, () => {
    console.log('listening on *:2222');
});